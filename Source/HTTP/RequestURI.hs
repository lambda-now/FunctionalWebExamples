{-# LANGUAGE OverloadedStrings #-}
module HTTP.RequestUri where

import qualified Network.Wai as NW
import qualified Network.Wai.Handler.Warp as WP
import qualified Network.HTTP.Types as HT
import qualified Network.HTTP.Types.Header as HD
import qualified Data.ByteString.Lazy as LBS

app request respond =
    let 
        hr = NW.responseLBS HT.status200 hdrs body
        hdrs = [(HD.hContentType, "text/plain")]
        body = LBS.fromStrict $ NW.rawPathInfo request
    in
        respond hr

requestUri = WP.run 3003 app
