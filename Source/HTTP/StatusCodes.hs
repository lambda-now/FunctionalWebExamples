{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative
import           Snap.Core
import           Snap.Http.Server

main :: IO ()
main = quickHttpServe $ route [
  ("ok", setStat 200 "OK"),
  ("unauthorized", setStat 401 "Unauthorized"),
  ("forbidden", setStat 403 "Forbidden"),
  ("500", setStat 500 "Internal Server Error")]
       <|> notFound
    where
        notFound = setStat 404 "Not Found"
        setStat c m = do
          modifyResponse $ setResponseCode c
          writeBS m
