{-# LANGUAGE OverloadedStrings #-}

module HTTP.RequestHeader where

import qualified Network.Wai as NW
import qualified Network.Wai.Handler.Warp as WP
import qualified Network.HTTP.Types as HT
import qualified Network.HTTP.Types.Header as HD
import qualified Data.ByteString.Lazy as LBS
import Control.Applicative ((<$>))

app request respond =
    let
        hr = NW.responseLBS HT.status200 hdrs body
        body = maybe "No Content-Type in request." id ct
        hdrs = [(HD.hContentType, "text/plain")]
        ct = LBS.fromStrict <$> lookup HD.hContentType rhdrs
        rhdrs = NW.requestHeaders request
    in
        respond hr

requestHeaders = WP.run 3002 app
