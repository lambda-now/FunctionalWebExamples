{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Applicative
import           Snap.Core
import           Snap.Http.Server
import           Snap.Util.FileServe

main :: IO ()
main = quickHttpServe $ route [
    ("/",
     ifTop $ serveFileAs "text/html" indexPath),
    ("/logo.png",
     serveFileAs "image/png" logoPath)]
       <|> notFound
        where
            indexPath = "static/ctindex.html"
            logoPath = "static/images/lambda.png"

            notFound = do
                modifyResponse $ setResponseCode 404
                writeBS "Not found."
