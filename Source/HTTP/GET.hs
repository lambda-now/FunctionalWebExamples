{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Snap.Core
import           Snap.Http.Server
import           Snap.Util.FileServe

main :: IO ()
main = quickHttpServe $ route [
    ("",
     ifTop $ serveFileAs "text/html" indexPath),
    ("/logo.png",
     method GET $ serveLogo)]
    where
        serveLogo =
            serveFileAs "static/images" logoPath
          
        indexPath = "static/getindex.html"
        logoPath = "static/images/lambda.png"
