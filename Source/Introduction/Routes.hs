{-# LANGUAGE OverloadedStrings #-}

module Introduction.Routes where

import Network.Wai (responseLBS, pathInfo)
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Types (status200)
import Network.HTTP.Types.Header (hContentType)

routes = run 3001 root

root request respond =
    case (pathInfo request) of
        ["updog"] -> plainResponse request respond "Updog"
        _ -> plainResponse request respond "What's"

plainResponse _ respond body =
    let
        hdrs = [(hContentType, "text/plain")]
    in
        respond $ responseLBS status200 hdrs body
        

